[forked from https://github.com/siyandong/CoScan.git]

# Multi-Robot Collaborative Dense Scene Reconstruction (SIGGRAPH 2019)
We provide the implementation of the optimal-mass-transport algorithm tailored for collaborative scanning along with virtual scan examples, on top of ROS.

# Requirements
This package depends on OpenCV, CGAL and OctoMap. 
- Please, install OpenCV and Octomap 
- CGAL will be automatically installed by the coscan package CMake file

# Download and Installation
```
catkin_ws/src$ git clone https://github.com/siyandong/CoScan.git
```
~~catkin_ws$ catkin_make_isolated~~ 
```
catkin_ws/src$ cd coscan
catkin_ws/src/coscan$ ./set_gazebo_mp1_world.sh
```
```
catkin_ws/src/coscan$ cd ../..
catkin_ws$ catkin_make 
```

# Run


Simple (all in one): 
```
$ rosrun virtual_scan sim_launcher 
```
```
$ rosrun virtual_scan kill_sim    #in order to kill the simulation and all nodes  
```
Detailed: 
```
$ roslaunch virtual_scan simulator.launch

$ roslaunch virtual_scan data_server.launch

$ rosrun co_scan co_scan
```

# Issues

## Gazebo: Models 

From https://github.com/jaejunlee0538/gazebo_2.2_ros_indigo/issues/1  

Run the following command to download gazebo model repository.
```
$ wget -r -R "index\.html*" http://models.gazebosim.org/
```
It will take long (in my case 59m 29s).
Then you can find a `models.gazebosim.org` folder.

You have to copy all the model files into `~/.gazebo/models/` folder.

### Gazeo: Error in REST request

```
[Err] [REST.cc:205] Error in REST request
```
This can be fixed by changing the url line in `~/.ignition/fuel/config.yaml` to `https://api.ignitionrobotics.org`

# Citation
If you use this code for your research, please cite our paper:
```
@article{dong2019multi,
  title={Multi-robot collaborative dense scene reconstruction},
  author={Dong, Siyan and Xu, Kai and Zhou, Qiang and Tagliasacchi, Andrea and Xin, Shiqing and Nie{\ss}ner, Matthias and Chen, Baoquan},
  journal={ACM Transactions on Graphics (TOG)},
  volume={38},
  number={4},
  pages={84},
  year={2019},
  publisher={ACM}
}
```
