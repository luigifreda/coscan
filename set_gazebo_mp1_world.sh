#!/usr/bin/env bash

set -e 

cp -r virtual_scan/worlds/mp1 ~/.gazebo/models

echo 'copied world model in ~/.gazebo/models/mp1'
ls ~/.gazebo/models/mp1 
