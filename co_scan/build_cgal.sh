#!/usr/bin/env bash

STARTING_DIR=`pwd`

if [ ! -d thirdparty ]; then
	mkdir thirdparty 
fi  

if [ ! -d thirdparty/cgal ]; then
	cd thirdparty
	touch CATKIN_IGNORE 
	git clone --branch releases/CGAL-4.9.1 https://github.com/CGAL/cgal cgal 
	cd cgal 
	if [ ! -d build ]; then
		mkdir build 
	fi  
	cd build
	cmake -DCMAKE_BUILD_TYPE=Release ..
	make -j 4	
fi 

cd $STARTING_DIR 
